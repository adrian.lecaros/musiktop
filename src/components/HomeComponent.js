import React, { Component } from 'react'
import { Card, CardBody, CardHeader, CardImg, CardImgOverlay, CardTitle, Col, Row } from 'reactstrap'
import { singlesLoading } from '../redux/ActionCreators'
import { Loading } from './LoadingComponent'

class Home extends Component {
    constructor (props) {
        super(props)
    }

    render () {
        const RenderTop2to5 = ({musicList, type}) => (
            musicList.map(musicElement =>
            (<Card>
                <Row>
                    <Col sm="3">
                        <CardImg width="100%" src={`assets/images/${type}/${musicElement.id}.jfif`} alt={musicElement.id} />
                    </Col>
                    <Col sm="2">
                        <CardBody>
                            <h3>{musicElement.thisweek}</h3>
                        </CardBody>
                    </Col>
                    <Col sm="7">
                        <CardBody>
                            <h5>{musicElement.title}</h5>
                            <p>{musicElement.artist}</p>
                        </CardBody>
                    </Col>
                </Row>
            </Card>))
        )

        const RenderMusicCard = ({musicList, type}) => (
            <Card>
                <CardHeader>
                <Row>
                    <Col sm="4">
                        <CardImg width="100%" src={`assets/images/${type}/${musicList[0].id}.jfif`} alt={musicList[0].id} />    
                    </Col>
                    <Col sm="6">
                        <h2>{musicList[0].thisweek}</h2>        
                        <h5>{musicList[0].title}</h5>
                        <p>{musicList[0].artist}</p>
                    </Col>
                </Row>
                </CardHeader>
                <CardBody>
                    <RenderTop2to5 musicList={musicList.filter(musicElement => parseInt(musicElement.thisweek) > 1 && parseInt(musicElement.thisweek) <= 5)}
                                    type={type}/>
                </CardBody>
            </Card>
        )

        if (this.props.singlesLoading || this.props.albumsLoading) {
            return (
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            )
        }
        return (
            <React.Fragment>
                <div className="container">
                    <div className="row mt-4 mb-4">
                        <div className="col-6">
                            <h3 className="mb-4">Top Singles</h3>
                            <RenderMusicCard musicList={this.props.singles}
                                                type="singles" />
                        </div>
                        <div className="col-6">
                            <h3 className="mb-4">Top Albums</h3>
                            <RenderMusicCard musicList={this.props.albums}
                                                type="albums" />
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Home