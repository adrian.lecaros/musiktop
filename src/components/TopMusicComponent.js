import React, { Component } from 'react'
import { Card, CardImg, CardBody, Col, Row } from 'reactstrap'
import { ClockFill, TreeFill, ReplyFill  } from 'react-bootstrap-icons'
import { Loading } from './LoadingComponent'

class TopMusic extends Component {
    constructor (props) {
        super(props)
    }

    render () {
        const RenderArrow = ({thisweek, lastweek}) => {
            let arrow = 'right'
            if (thisweek > lastweek)
                arrow = 'up'
            else if (thisweek > lastweek) {
                arrow = 'down' 
            }
            return <span className={`fa fa-arrow-${arrow} fa-lg`}></span>
        }

        const RenderMusicCard = ({musicList, type}) => (
            musicList.map(musicElement =>
                (<Card>
                    <Row>
                        <Col sm="2">
                            <CardImg width="100%" src={`assets/images/${type}/${musicElement.id}.jfif`} alt={musicElement.id} />
                        </Col>
                        <Col sm="2">
                            <CardBody>
                                <h2>{musicElement.thisweek}</h2>
                                {musicElement.lastweek === '-' ?
                                    <span className={`fa fa-star fa-lg`}></span> :
                                    <RenderArrow thisweek={parseInt(musicElement.thisweek)}
                                                lastweek={parseInt(musicElement.lastweek)}/>
                                 }
                            </CardBody>
                        </Col>
                        <Col sm="5">
                            <CardBody>
                                <h2>{musicElement.title}</h2>
                                <p>{musicElement.artist}</p>
                            </CardBody>
                        </Col>
                        <Col sm="1">
                            <CardBody>
                                <ReplyFill size="lg" />
                                <Row className="justify-content-center">
                                    <Col sm="auto">
                                        <h5 className="mt-4">{musicElement.lastweek}</h5>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Col>
                        <Col sm="1">
                            <CardBody>
                                <TreeFill size="lg"/>
                                <Row className="justify-content-center">
                                    <Col sm="auto">
                                        <h5 className="mt-4">{musicElement.peak}</h5>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Col>
                        <Col sm="1">
                            <CardBody>
                                <ClockFill size="lg" />
                                <Row className="justify-content-center">
                                    <Col sm="auto">
                                        <h5 className="mt-4">{musicElement.totalweeks}</h5>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Col>
                    </Row>
                </Card>))
        )

        if (this.props.isLoading) {
            return (
                <div className="container">
                    <div className="row">
                        <Loading />
                    </div>
                </div>
            )
        }

        return (
            <React.Fragment>
                <div className="container">
                    <div className="row mt-4 mb-4">
                        <div className="col-12">
                            <h1 className="mb-5">{`Top ${this.props.type} of the week`}</h1>
                            <RenderMusicCard musicList={this.props.musicList}
                                                type={this.props.type} />
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default TopMusic