import React, { Component } from 'react'
import { Switch, Route, Redirect, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { fetchAlbums, fetchSingles } from '../redux/ActionCreators'
import Home from './HomeComponent'
import TopMusic from './TopMusicComponent'
import Footer from './FooterComponent'
import Header from './HeaderComponent'

const mapStateToProps = state => {
    return {
        singles: state.singles,
        albums: state.albums
    }
}

const mapDispatchToProps = (dispatch) => ({
    fetchSingles: () => dispatch(fetchSingles()),
    fetchAlbums: () => dispatch(fetchAlbums())
})

class Main extends Component {
    constructor (props) {
        super(props)
    }

    componentDidMount() {
        this.props.fetchSingles()
        this.props.fetchAlbums()
    }

    render () {
        const HomePage = () => ( // This is where the Home Component will be rendered
            <Home singles={this.props.singles.singles}
                    singlesLoading={this.props.singles.isLoading}
                    albums={this.props.albums.albums}
                    albumsLoading={this.props.albums.isLoading} />
        )

        const TopMusicPage = ({musicList, isLoading, type}) => (
            <TopMusic musicList={musicList}
                        isLoading={isLoading}
                        type={type} />
        )

        return (
            <React.Fragment>
                <Header />
                    <Switch>
                        <Route path="/home" component={HomePage} />
                        <Route path="/topSingles" component={() => <TopMusicPage musicList={this.props.singles.singles}
                                                                                    isLoading={this.props.singles.isLoading}
                                                                                    type='singles' />} />
                        <Route path="/topAlbums" component={() => <TopMusicPage musicList={this.props.albums.albums}
                                                                                        isLoading={this.props.albums.isLoading}
                                                                                        type='albums' />} />
                        <Redirect to="/home" />
                    </Switch>
                <Footer />
            </React.Fragment>
        )
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main))