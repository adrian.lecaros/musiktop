import * as ActionTypes from './ActionTypes'

export const Singles = (state = {
    isLoading: true,
    singles: []
    }, action) => {
        switch(action.type) {
            case ActionTypes.ADD_SINGLES:
                return {...state, isLoading: false, singles: action.payload}
            case ActionTypes.SINGLES_LOADING:
                return {...state, isLoading: true, singles: []}
            default:
                return state
        }
    }