import * as ActionTypes from './ActionTypes'
import { SINGLES } from '../shared/singles'
import { ALBUMS } from '../shared/albums'

export const fetchSingles = () => (dispatch) => {
    dispatch(singlesLoading(true))

    setTimeout(() => console.log('Fetch Singles Timeout'), 2000)

    return dispatch(addSingles(SINGLES))
}

export const fetchAlbums = () => (dispatch) => {
    dispatch(albumsLoading(true))

    setTimeout(() => console.log('Fetch Albums Timeout'), 2000)

    return dispatch(addAlbums(ALBUMS))
}

export const addSingles = (singles) => ({
    type: ActionTypes.ADD_SINGLES,
    payload: singles
})

export const addAlbums = (albums) => ({
    type: ActionTypes.ADD_ALBUMS,
    payload: albums
})

export const singlesLoading = () => ({
    type: ActionTypes.SINGLES_LOADING
})

export const albumsLoading = () => ({
    type: ActionTypes.ALBUMS_LOADING
})