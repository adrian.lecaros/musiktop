import * as ActionTypes from './ActionTypes'

export const Albums = (state = {
    isLoading: true,
    albums: []
    }, action) => {
        switch(action.type) {
            case ActionTypes.ADD_ALBUMS:
                return {...state, isLoading: false, albums: action.payload}
            case ActionTypes.ALBUMS_LOADING:
                return {...state, isLoading: true, albums: []}
            default:
                return state
        }
    }