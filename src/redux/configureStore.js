import { Singles } from './singles'
import { Albums } from "./albums"
import { applyMiddleware, combineReducers, createStore } from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'

export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            singles: Singles,
            albums: Albums
        }),
        applyMiddleware(thunk, logger)
    )

    return store
}